﻿using Entity.Entity;
using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.UserInfo
{
    public interface IUserInfoRepository
    {
        Response<UserViewModel> GetUserByUserName(UserViewModel model);
    }
}
