﻿using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Model.Company;
using Model.Requests;
using Model.Response;
using System;
using System.Net.Http;

namespace fsb.smartcontract.Services.Company
{
    public class CompanyService : Service<ICompanySevice>, ICompanySevice
    {
        public CompanyService(
            CallWebAPI api, 
            Microsoft.Extensions.Logging.ILogger<ICompanySevice> logger,
            IHttpClientFactory factory, 
            IHttpContextAccessor context
            ) : base(api, logger, factory, context)
        {
        }

        public Response<string> CreateCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.COMPANY_CREATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<string> DeleteCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.COMPANY_DELETE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<CompanyModel> GetCompanyById(CompanyModel model)
        {
            Response<CompanyModel> res = new Response<CompanyModel>();
            try
            {
                string url = Path.COMPANY_BYID;
                res = _api.Post<Response<CompanyModel>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public TableResponse<CompanyViewModel> GetListCompany(SearchCompanyModel search)
        {
            TableResponse<CompanyViewModel> res = new TableResponse<CompanyViewModel>();

            try
            {
                string url = Path.COMPANY_DATATABLE;
                res = _api.Post<TableResponse<CompanyViewModel>>(url, search);
                if (res.Code != StatusCodes.Status200OK)
                {
                    res.RecordsFiltered = 0;
                    res.RecordsTotal = 0;
                    res.Draw = search.Draw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }

            return res;
        }

        public Response<string> UpdateCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.COMPANY_UPDATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
    }
}
