﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Company;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Company
{
    public class CompanyRepository : Repository<CompanyRepository>, ICompanyRepository
    {
        private readonly IConfiguration _configuration;
        public CompanyRepository(
            FsbSmartContractContext context, 
            IConfiguration configuration,ILogger<CompanyRepository> logger ) : base(context, logger)
        {
            _configuration = configuration;
        }

        public Response<string> CreateCompany(CompanyModel model) //Thêm Công Ty
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.CompanyName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên công ty không được bỏ trống!";
                    return res;
                }

                if (string.IsNullOrEmpty(model.CompanyCode))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Mã công ty không được bỏ trống!";
                    return res;
                }

                var data = _context.Companies.FirstOrDefault(x => x.CompanyName == model.CompanyName.TrimEnd().TrimStart());
                if (data != null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Công ty đã tồn tại!";
                    return res;
                }

                Entity.Entity.Company company = new Entity.Entity.Company();
                company.CompanyName = model.CompanyName;
                company.CompanyCode = model.CompanyCode;


                _context.Companies.Add(company);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm công ty thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm tên công ty!";
            }

            return res;
        }

        public Response<string> DeleteCompany(CompanyModel model) //Xóa công ty 
        {
            Response<string> res = new Response<string>();

            try
            {
                var company = _context.Companies.FirstOrDefault(x => x.Id == model.Id && x.IsDeleted == false);
                if (company == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại công ty, không thể xóa!";
                    return res;
                }
                company.IsDeleted = true;

                _context.Companies.Update(company);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Xóa công ty thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi xóa công  ty!";
            }

            return res;
        }

        public Response<CompanyModel> GetCompanyById(CompanyModel model) //Lấy thông tin công  ty
        {
            Response<CompanyModel> res = new Response<CompanyModel>();

            try
            {
                var query = (from a in _context.Companies
                             where a.IsDeleted == false && a.Id == model.Id
                             select new
                             {
                                 a
                             }).ToList();

                var group = query.GroupBy(x => x.a.Id).Select(group => new CompanyModel
                {
                    Id = group.Key,
                    CompanyCode = group.ToList().FirstOrDefault().a.CompanyCode,
                    CompanyName = group.ToList().FirstOrDefault().a.CompanyName
                }).ToList();

                res.Code = StatusCodes.Status200OK;
                res.Data = group.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin công ty!";
            }

            return res;
        }

        public TableResponse<CompanyViewModel> GetListCompany(SearchCompanyModel search)
        {
            TableResponse<CompanyViewModel> result = new TableResponse<CompanyViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.Companies.Where(x => x.IsDeleted == false).Select(x => new CompanyViewModel
                {
                    Id = x.Id,
                    CompanyCode = x.CompanyCode,
                    CompanyName = x.CompanyName
                }).ToList();

                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.CompanyName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách công ty!";
            }
            return result;
        }

        public Response<string> UpdateCompany(CompanyModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.CompanyCode))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Mã công ty không được bỏ trống!";
                    return res;
                }

                if (string.IsNullOrEmpty(model.CompanyName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên công ty không được bỏ trống!";
                    return res;
                }

                var company = _context.Companies.FirstOrDefault(x => x.IsDeleted == false && x.Id == model.Id);
                if (company == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại công ty, không thể cập nhật!";
                    return res;
                }

                company.CompanyName = model.CompanyName;
                company.CompanyCode = model.CompanyCode;
                _context.Companies.Update(company);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Sửa công ty thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi sửa công ty!";
            }

            return res;
        }
    }
}
