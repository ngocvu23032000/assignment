﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Response;
using Model.UserInfo;
using Repository.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserInfoController : BaseController
    {

        private readonly IUserInfoRepository _userInfoRepository;
        private IWebHostEnvironment _hostingEnvironment;

        public UserInfoController(
            IUserInfoRepository userInfoRepository,
            ILogger<UserInfoController> logger,
            IConfiguration configuration,
            IWebHostEnvironment webHostEnvironment
            ) : base(configuration, logger)
        {
            _userInfoRepository = userInfoRepository;
            _hostingEnvironment = webHostEnvironment;
        }

        [Route("GetUserByUserName")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN, RoleConstants.ACCOUNTANT_HO, RoleConstants.ACCOUNTANT_BP, RoleConstants.ACCOUNTANT_INSTITUTE)]
        public Response<UserViewModel> GetUserByUserName(UserViewModel model)
        {
            return _userInfoRepository.GetUserByUserName(model);
        }
    }
}
