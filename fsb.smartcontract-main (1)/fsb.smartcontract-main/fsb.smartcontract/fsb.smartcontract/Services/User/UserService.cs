﻿using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.User
{
    public class UserService : Service<UserService>, IUserService
    {
        public UserService(
           CallWebAPI api,
           ILogger<UserService> logger,
           IHttpClientFactory factory,
           IHttpContextAccessor context
           ) : base(api, logger, factory, context)
        {
        }

        public Response<UserViewModel> GetUserByUserName(UserViewModel model)
        {
            Response<UserViewModel> res = new Response<UserViewModel>();
            try
            {
                string url = Path.USER_BYUSERNAME;
                res = _api.Post<Response<UserViewModel>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
    }
}
